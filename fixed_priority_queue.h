/*
 * @author: Jens Bergman
 * @date:   2016-08-18
 * @info:   A wrapper class for the priority queue
 *          with a fixed size that's determined in
 *          the constructor.
 */

#ifndef FIXED_PRIORITY_QUEUE
#define FIXED_PRIORITY_QUEUE

#include <queue>

/* We want to keep the properties of the priority queue.
 * - T is the type that's should be in the queue
 * - Container is the underlying structure used (priority_queue is an adapter class)
 * - Compare gives the ability to decide the order of the queue
 */
template <class T, class Container = std::vector<T>,
                   class Compare = std::less<T> >

/*
 * Constructors, desctructor, friend functions and assignment operator,
 * but are automatically generated (cannot be done if class have
 * pointers as members).
*/
class fixed_priority_queue: public std::priority_queue<T, Container, Compare>
{
protected:
    // maximum size of queue (typename assure correct type of variable)
    typename std::priority_queue<T, Container, Compare>::size_type max_size;
    typename Container::size_type getMaxSize() const;

public:
    fixed_priority_queue(int max_size);
    fixed_priority_queue(const std::priority_queue<T, Container, Compare>&old_queue, int max_size);
    fixed_priority_queue(const fixed_priority_queue<T, Container, Compare> &old_queue);
    fixed_priority_queue<T, Container, Compare>& operator=
        (const fixed_priority_queue<T, Container, Compare> &old_queue);

    // modified version of push (keeps the size of the queue)
    void push(const T& val);
    // get the underlying container (vector is standard)
    Container getQueue() const;

    // make priority_queue functions visible
    using std::priority_queue<T, Container, Compare>::pop;
    using std::priority_queue<T, Container, Compare>::empty;
    using std::priority_queue<T, Container, Compare>::size;
    using std::priority_queue<T, Container, Compare>::top;
};
// ----PRIVATE MEMBER FUNCTIONS ---------

// ---CONSTRUCTORS -----------------
template <class T, class Container, class Compare>
fixed_priority_queue<T, Container, Compare>::fixed_priority_queue
(int max_size):
    std::priority_queue<T, Container, Compare>(),
    max_size(max_size){}

template <class T, class Container, class Compare>
fixed_priority_queue<T, Container, Compare>::fixed_priority_queue(
        const std::priority_queue<T, Container, Compare> &old_queue,
        int max_size):
    std::priority_queue<T, Container, Compare>(old_queue),
    max_size(max_size)
{
    std::cout << "inside constructor with max_size: " << this->max_size << "\n";
    // pop the queue until size is right
    int diff =  std::priority_queue<T, Container, Compare>::size() - max_size;
    for(int i = 0; i < diff; i++){
        std::priority_queue<T, Container, Compare>::pop();
    }
}

template <class T, class Container, class Compare>
fixed_priority_queue<T, Container, Compare>::fixed_priority_queue(
    const fixed_priority_queue<T, Container, Compare> &old_queue):
    std::priority_queue<T, Container, Compare>(),
    max_size(old_queue.max_size)
{
    // copy underlying container
    std::priority_queue<T, Container, Compare>::c = old_queue.getQueue();
}

// ---- OVERWRITTEN OPERATORS -------------
template <class T, class Container, class Compare>
fixed_priority_queue<T, Container, Compare>&
fixed_priority_queue<T, Container, Compare>::operator=
(const fixed_priority_queue<T, Container, Compare> &old_queue)
{
    if(this != &old_queue) // no copy to itself
    {
        max_size = old_queue.getMaxSize();
        std::priority_queue<T, Container, Compare>::c = old_queue.getQueue();
    }
    return *this;
}

// ----- PUBLIC MEMBER FUNCTIONS ----------------------
template <class T, class Container, class Compare>
void fixed_priority_queue<T, Container, Compare>::push(const T& val)
{
    // if queue size is less than max, then we just add element
    if( std::priority_queue<T, Container, Compare>::size() < max_size)
    {
        std::priority_queue<T, Container, Compare>::push(val);
    }
    // if queue size is already full we add element if comparison
    // returns true
    else if (
            std::priority_queue<T, Container, Compare>::comp(val,
                std::priority_queue<T, Container, Compare>::top() )
            )
    {
        std::priority_queue<T, Container, Compare>::push(val);
        std::priority_queue<T, Container, Compare>::pop();
    }
}

template <class T, class Container, class Compare>
Container fixed_priority_queue<T, Container, Compare>::getQueue() const
{
    //return underlying container
    return std::priority_queue< T, Container, Compare >::c;
}

// ----PRIVATE MEMBER FUNCTIONS ---------
template <class T, class Container, class Compare>
typename Container::size_type
fixed_priority_queue<T, Container, Compare>::getMaxSize() const
{
    //return max size
    return max_size;
}

#endif
