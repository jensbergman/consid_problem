# README #
## The consid-problem ##

Under en intervju fick jag denna fråga vilket var ett intressant problem.

### The problem ###
Consid-problemet går ut på att hitta de 5 mest förekommande nummerplåtarna ur en fil med 7 miljoner nummerplåtar. Problemet kan delas upp två problem:

 * Läsa alla nummerplåtar samt inkremetera dem.

 * Göra en topp-lista.

Problemet blir snabbt komplex om man istället försöker sortera alla nummerplåtar direkt. Om man lägger till en nummerplåt i en datastruktur i en viss ordning så blir det ineffektivt att inkrementera antalet nummerplåtar (då man måste leta hitta elementet med samma nummerplåtar).

### Lösning: problem 1 ###
För att effektivt inkrementera antalet av ett specifik nummerplåt så måste dess element hittas effektivt. Det finns 10^3*26^3 = 17 576 000 möjliga kombinationer av en nummerplåt. Om varje element representerar antalet av en nummerplåt och är 1 B stor så är den maximala storleken ca 17,5 MB.
   
Det finns två bra kandidater av datastrukturer:

 * Hash tabell: Med tidskomplexitet O(1) så är det en lämplig kandidat. Observera dock att tidskomplexiteten inte är garanterad (beror bl.a. på storlek av tabell). Tabellen skulle kunna vara endast 7 MB med risk för att den blir ineffektiv. Det är svårt att avgöra vilken storlek som är lämplig, speciellt om man tar hänsyn till att antalet nummerplåtar i filen kan öka.

 * Array: Ett element i en array kan representera en nummerplåt (kräver ca 17,5 MB), t.ex. första element representerar AAA000 och sista elementet ZZZ999. Detta gör att ett element kan hittas med tidskomplexitet O(1). En array är lika effektiv oavsett om antalet nummerplåtar i filen ökar i framtiden och gör den till den mest lämpade strukturen.

### Lösning: problem 2 ###

För att effektivt göra en topp-lista så kan den befintliga arrayen sorteras eller kan en separat lista skapas. Sorteringsalgoritmer har ofta tidskomplexitet O(n*log(n)) i "worst-case" (eftersom arrayen har en statisk storlek får vi O(n*17)). En X-topplista däremot har O(n*log(X)) med en prioritetskö där endast en operation behövs för de element mindre än de som redan är i topplistan (vilket är till stor fördel då 10,5 miljoner nummerplåtar inte kommer kunna läggas in i topplistan).

### How do I get set up? ###
* Generera fil med nummer-plåtar:  
Kompilera generate.c och kör med argumenten <output_file> och <antalet_plåtar>.
* Kompilera lösningen:  
Kör make för att kompilera projektet och kör 'main' i mappen 'out'. 'main' tar två argument: <input_file> och <toplista_storlek>. Kommentera/kommentera av definitionen 'TIME' i main för att välja mellan utskrift och tidsmätning.
