#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <functional> // greater functor
#include <utility> // pair
#include <sstream> // reading integers from command line
#include <time.h> // calculating execution time

#include "fixed_priority_queue.h"

//#define TIME // if defined prints out executions time (for debugging)

/* Element calculation:
 * AAA000 = 1
 * AAA009 = 10          (0+10¹)
 * AAA099 = 100         (0+10²)
 * AAA999 = 1000        (0+10³)
 * AAZ999 = 26000       (0+26*10^3)
 * AZZ999 = 676000      (0+26^2*10^3)
 * ZZZ999 = 17576000    (0+26^3*10^3)
 */
int get_index(char* plate)
{
    int index = (int) plate[5]-48;
    index += (int) (plate[4]-48)*10;
    index += (int) (plate[3]-48)*100;
    index += (int) (plate[2]-65)*1000;
    index += (int) (plate[1]-65)*26000;
    index += (int) (plate[0]-65)*676000;

    return index;
}

void calculate_letter(char &letter, int &index, int divisor)
{
    letter += index/divisor;
    index -= ((int) index/divisor)*divisor;
}

// reverse function of get_index (index -> plate)
std::string get_plate(int index)
{
    std::string plate = "AAA000";
    calculate_letter(plate[0], index, 676000);
    calculate_letter(plate[1], index, 26000);
    calculate_letter(plate[2], index, 1000);
    calculate_letter(plate[3], index, 100);
    calculate_letter(plate[4], index, 10);
    calculate_letter(plate[5], index, 1);
    return plate;
}

/* Creating array where each element is the amount of a
 * particular number plate
 */
    template <typename T>
void get_plates(std::string filename, std::vector<T> &plates)
{
    char divider;
    char *plate = new char[6];
    std::ifstream file(filename, std::ifstream::binary);
    // read plates
    while(file.get(plate,7)){
        file.get(divider); // remove character between plates
        // update # occurances for current plate
        plates[get_index(plate)] += 1;
    }
    delete[] plate;
}

/*usage main <filename> <toplistsize> */
int main(int argc, char ** argv)
{
    if(argc < 3){
        std::cout << "Usage: main <filename> <toplist-size>\n";
        return 0;
    }
    std::istringstream ss(argv[2]);
    int toplist_size;
    if(!(ss >> toplist_size))
    {
        std::cout << "invalid number " << argv[2] << std::endl;
    }
    std::string filename = argv[1];

#ifdef TIME
    // initiating total time calculation
    const clock_t begin_time = clock();
    // partial time calculation
    clock_t begin_time_part = clock();
#endif

    // array to store amounts of each plate
    std::vector<int> plates(17576000);
    // calculate how many times a plate occurs
    get_plates(filename, plates);

#ifdef TIME
    std::cout << "Calculation of occurances execution time: " <<
        float(clock() - begin_time_part)/CLOCKS_PER_SEC << std::endl;

    begin_time_part = clock();
#endif

    // create a top-list...
    fixed_priority_queue<std::pair<int,int>, std::vector< std::pair<int, int> >,
        std::greater< std::pair<int,int> > > queue(toplist_size);
    int tmp_plate = 0; // integer representation of plate
    for(auto iter = plates.begin(); iter != plates.end(); ++iter)
    {
        // avoid inserting zero-elements for speed
        if(plates[tmp_plate] > 0){
            // store in pair to remember both the number and amount of plate
            std::pair<int,int> plate(plates[tmp_plate], tmp_plate);
            // push plate into priority queue
            queue.push(plate);
        }
        tmp_plate++;// do the same for next plate
    }

#ifdef TIME
    std::cout << "Calculation of creating toplist execution time: " <<
        float(clock() - begin_time_part)/CLOCKS_PER_SEC << std::endl;

    begin_time_part = clock();
#endif

    std::cout << "Priority queue contains:\n";
    std::vector< std::pair<int,int> > ordered_queue = queue.getQueue();
    // popping priority_queue gives ascending order, therefore we
    // sort top-list in descending order
    std::sort(ordered_queue.begin(), ordered_queue.end(),
            std::greater< std::pair<int, int> >());
#ifndef TIME // doesn't print out toplist to simply debugging
    // print top list in descending order
    int toplist_place = 1;
    for(auto iter = ordered_queue.begin(); iter < ordered_queue.end(); iter++)
    {
        std::cout << toplist_place << ". Plate " <<
            get_plate( std::get<1>(*iter) ) <<
            " : " << std::get<0>(*iter) << std::endl;
        toplist_place++;
    }
#endif
#ifdef TIME
    std::cout << "Calculation of sorting toplist execution time: " <<
        float(clock() - begin_time_part)/CLOCKS_PER_SEC << std::endl;

    // check execution time
    const clock_t end_time = clock();
    std::cout << "Final execution time (clock ticks): " << (end_time - begin_time) << std::endl;
    std::cout << "Final execution time (seconds): " << float(end_time - begin_time)/CLOCKS_PER_SEC << std::endl;

//    std::cout << "comparing to sorting directly...\n";
//    begin_time_part = clock();
//    std::sort(plates.rbegin(), plates.rend());
//    std::cout << "Execution time: " <<
//    float(clock() - begin_time_part)/CLOCKS_PER_SEC << std::endl;
#endif

    return 0;
}
