#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

// generate a license plate number (AAA111)
char * generatePlate(char *plate);

int main(int argc, char ** argv){

    if(argc < 3){
        printf( "usage: <output_file> <#plates>");
        return 0;
    }
    int amount = atoi( argv[2] );
    printf("Filename: %s, amount %d", argv[1], atoi(argv[2]));

	// open a file with write permission
	FILE *f = fopen(argv[1], "w");
	if (f == NULL){
	    printf("Error opening file!\n");
	    exit(1);
	}

	// get seed for rand function
	srand(time(NULL));

	char plate[6];
	// generate 7.000.000 license plates
	for(int i = 0; i < amount; i++){
		// generate new plate
		generatePlate(plate);
		// print new plate to file directly
		fprintf(f, "%s;", plate);
	}
	// close file
	fclose(f);
}

char* generatePlate(char *plate){
	// generate the three first letters (english alphabet)
	for(int i = 0; i < 3; i++){
		// generate number between 0-25 and adds 65 to get a upper case letter
		plate[i] = (rand()%26)+65;
	}
	// generate the last three numbers
	for(int i = 3; i < 6; i++){
		// generate number between 0-9 and adds 48 to get ASCII number
		plate[i] = (rand()%10)+48;
	}

	return plate;
}

