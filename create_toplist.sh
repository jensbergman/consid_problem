#!/bin/sh

#info:
#Calculates number of occurances of words and then outputs a
#toplist of specified length

#help-message
usage() { echo "Usage: $0 [-f <filename>] <size>" 1>&2; exit 1; }

OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
output_file=""

while getopts "h?f:" opt; do
    case "$opt" in
        h|\?)
            usage
            exit 0
            ;;
        f)  output_file=$OPTARG
            ;;
        *)  usage
            ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

if [ -z "${output_file}" ] || [ $1 -lt 0 ]; then
    usage
fi

tr -c '[:alnum:]' '[\n*]' < $output_file | sort | uniq -c | sort -nr | head  -$@
