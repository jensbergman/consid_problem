GCC = g++
OBJDIR	= obj
OUTDIR	= out
TARGET	= main
#machine code (linked together by linker)
OBJECTS = $(OBJDIR)/$(TARGET).o

CPPFLAGS += -g

CXXFLAGS += -Wall -Wextra -std=c++11 -g

all: folders $(TARGET)

folders:
	mkdir -p obj out

clean:
	rm -r obj out

# create output
$(TARGET): $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(OBJECTS) -o $(OUTDIR)/$(TARGET)

#create machine code (objects) from each file
$(OBJDIR)/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@
